project(wg_filter)
cmake_minimum_required(VERSION 3.15)
set(AUTHOR "wonguk.lee")

#https://cmake.org/cmake/help/latest/prop_gbl/ALLOW_DUPLICATE_CUSTOM_TARGETS.html
set_property(GLOBAL PROPERTY ALLOW_DUPLICATE_CUSTOM_TARGETS 1)

# find KERNEL_RELEASE
# https://github.com/enginning/cmake-kernel-module/blob/main/CMakeLists.txt
execute_process(
		COMMAND uname -r
		OUTPUT_VARIABLE KERNEL_RELEASE
		OUTPUT_STRIP_TRAILING_WHITESPACE
)	

# find KERNEL_HEADERS
# https://gitioc.upc.edu/open-source/xenomai-cmake/blob/master/cmake-modules/FindKernelHeaders.cmake
find_path(KERNEL_HEADERS
		include/linux/user.h
		PATHS /usr/src/linux-headers-${KERNEL_RELEASE}
)

# find KERNEL_BUILD
message(STATUS "Kernel release : ${KERNEL_RELEASE}")
message(STATUS "Kernel headers : ${KERNEL_HEADERS}")

add_subdirectory(./driver)
add_subdirectory(./daemon)

set_property(DIRECTORY
		APPEND
		PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "CMakeCache.txt"
)
set_property(DIRECTORY
		APPEND
		PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "cmake_install.cmake"
)
set_property(DIRECTORY
		APPEND
		PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "Makefile"
)
set_property(DIRECTORY
		APPEND
		PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "CMakeFiles"
)

# add dummy target for make clean of root directory
add_custom_target(foo
)
