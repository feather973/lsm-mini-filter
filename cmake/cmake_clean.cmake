# use -P
# https://stackoverflow.com/a/36579881

message(STATUS "cmake_clean start")
set(cmake_generated ${CMAKE_BINARY_DIR}/CMakeCache.txt
					${CMAKE_BINARY_DIR}/cmake_install.cmake
					${CMAKE_BINARY_DIR}/Makefile
					${CMAKE_BINARY_DIR}/CMakeFiles
)
	
message(STATUS "clean : ${cmake_generated}")

foreach(file ${cmake_generated})
	if (EXISTS ${file})
		file(REMOVE_RECURSE ${file})
	endif()
endforeach(file)

#set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "{cmake_generated}")
