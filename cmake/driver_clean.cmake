# use -P
# https://stackoverflow.com/a/36579881

message(STATUS "driver_clean start")

file(GLOB_RECURSE driver_generated
	"${CMAKE_BINARY_DIR}/*.mod*"
	"${CMAKE_BINARY_DIR}/*.o"
	"${CMAKE_BINARY_DIR}/*.cmd"
	"${CMAKE_BINARY_DIR}/modules.order"
	"${CMAKE_BINARY_DIR}/Module.symvers"
	"${CMAKE_BINARY_DIR}/Kbuild"
)

message(STATUS "clean : ${driver_generated}")

foreach(file ${driver_generated})
	if (EXISTS ${file})
		file(REMOVE_RECURSE ${file})
	endif()
endforeach(file)

#set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "{driver_generated}")
