#include <linux/module.h>

static int __init initdriver(void)
{
	return 0;
}

static void __exit exitdriver(void)
{
	return;
}

module_init(initdriver);
module_exit(exitdriver);

// https://docs.kernel.org/process/license-rules.html
MODULE_LICENSE("GPL v2");
