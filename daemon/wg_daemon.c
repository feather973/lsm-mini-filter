#define _GNU_SOURCE
#include <pthread.h>
#include <sys/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>

int g_cpus;
pthread_t *g_tids;

static void *
receiver_thread(void *arg)
{
	int cpu = (int) arg;

	printf("thread (cpu:%d)\n", cpu);
	pthread_exit(NULL);
}

int set_affinity(pthread_t tid, int cpu)
{
	cpu_set_t cpuset;
	pthread_t thread;

	int ret;

	CPU_ZERO(&cpuset);
	CPU_SET(cpu, &cpuset);

	ret = pthread_setaffinity_np(tid, sizeof(cpuset), &cpuset);
	if (ret) {
		printf("set affinity fail\n");	
		return 1;
	}

	return 0;
}

int daemon_init()
{
	int i, ret;

	g_cpus = get_nprocs();
	
	g_tids = calloc(sizeof(pthread_t), g_cpus);
	if (!g_tids) {
		printf("calloc pthread_t array fail\n");
		goto err;
	}

	for (i=0; i<g_cpus; i++) {
		ret = pthread_create(&g_tids[i], NULL, &receiver_thread, i);
		if (ret) {
			printf("pthread_create fail (cpu:%d)\n", i);
			goto err;
		}	

		ret = set_affinity(g_tids[i], i);
		if (ret) {
			printf("set affinity fail (cpu:%d)\n", i);
			goto err;
		}
	}	

	return 0;
err:
	return 1;
}

int daemon_exit()
{
	int i, ret;

	for (i=0; i<g_cpus; i++) {
		ret = pthread_join(g_tids[i], NULL);
		if (ret) {
			printf("pthread_join fail (cpu:%d)\n", i);
			goto err;
		}
	}

	return 0;
err:
	return 1;
}

int main()
{
	int ret;
	
	ret = daemon_init();
	if (ret)
		goto err;

	ret = daemon_exit();
	if (ret)
		goto err;

	return 0;
err:
	return 1;
}
